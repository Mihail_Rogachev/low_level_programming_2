all: main

clean:
	rm -rf parser.tab.c parser.tab.h parser.output lex.yy.c main debug

parser: parser/parser.y
	bison -d parser/parser.y

parser_debug: parser/parser.y
	bison -d --verbose --debug parser.y

lexer: lexer/lexer.l
	flex lexer/lexer.l

main: parser lexer structures/structures.c main.c
	gcc parser.tab.c lex.yy.c structures/structures.c main.c -o main

debug: parser_debug lexer structures/structures.c main.c
	gcc parser.tab.c lex.yy.c structures/structures.c main.c -o debug

.PHONY: all clean parser parser_debug lexer
